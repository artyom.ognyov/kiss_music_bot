import telebot;
import re;
from telebot import types;
import requests;
import json;

bot = telebot.TeleBot('1386049675:AAFyRr-5CxgfW_5LijRkzc3axhuJ7HQko3s')

def getSpotifyToken():
	url = 'https://open.spotify.com/get_access_token'
	params = {
		'reason': 'transport',
		'productType': 'web_player',
	}
	r = requests.get(url, params = params)
	token = r.json()['accessToken']
	return token

def spotifySearch(query):
	url = 'https://api.spotify.com/v1/search'
	headers = {
		'Content-type': 'application/json; charset=UTF-8',
		'Authorization':'Bearer ' + getSpotifyToken()
	}
	data = {
		'type': 'album,artist,playlist,track,show_audio,episode_audio',
		'q': query,
		'decorate_restrictions': 'false',
		'best_match': 'true',
		'include_external': 'audio',
		'limit': '10',
		'market': 'RU',
	}
	r = requests.get(url, params = data, headers = headers).json()
	print("Len >> " + str(len(r['best_match']['items'])))
	if len(r['best_match']['items']) > 0:
		if 'images' in r['best_match']['items'][0] and len(r['best_match']['items'][0]['images']) > 0:
			numOfImages = len(r['best_match']['items'][0]['images'])
			return {
				'name': r['best_match']['items'][0]['name'],
				'type': r['best_match']['items'][0]['type'],
				'url': r['best_match']['items'][0]['external_urls']['spotify'],
				'image': r['best_match']['items'][0]['images'][numOfImages - 1]['url'],
				'image_height': r['best_match']['items'][0]['images'][numOfImages - 1]['height'],
				'image_width': r['best_match']['items'][0]['images'][numOfImages - 1]['width'],
				'error': False,
			}
		else:
			return {
				'name': r['best_match']['items'][0]['name'],
				'type': r['best_match']['items'][0]['type'],
				'url': r['best_match']['items'][0]['external_urls']['spotify'],
				'error': False,
			}
	else:
		return {
			'error': True,
		}

def appleMusicSearch(query):
	url = 'https://amp-api.music.apple.com/v1/catalog/us/search/suggestions'
	headers = {
		'Content-type': 'application/json; charset=UTF-8',
		'Authorization':'Bearer eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IldlYlBsYXlLaWQifQ.eyJpc3MiOiJBTVBXZWJQbGF5IiwiaWF0IjoxNjA4MjU0Njg2LCJleHAiOjE2MjM4MDY2ODZ9.8IHFWwdOMMrd7Yt3S4_pdWs4F3d8WQBQPNcetrz__XE2gPKb6EN7J95O-E7LaAf3lzLxoMkalYkyqzA-ssMTOw'
	}
	data = {
		'term': query,
		'types': 'albums,artists,songs,playlists,music-videos,activities,tv-episodes,editorial-items,stations',
		'kinds': 'topResults',
		'platform': 'web',
		'omit[resource]': 'autos',
		'limit[results:terms]': '5',
		'limit[results:topResults]': '10',
		'l': 'en-us',
	}
	r = requests.get(url, params = data, headers = headers).json()
	if True:
		return {'image': r['results']['suggestions'][0]['content']['attributes']['artwork']['url'] + "220x220bb-60.jpeg",
			'name': r['results']['suggestions'][0]['content']['attributes']['name'],
			'type': r['results']['suggestions'][0]['content']['type'][:-1],
			'url': r['results']['suggestions'][0]['content']['attributes']['url'],
			'image': r['results']['suggestions'][0]['content']['attributes']['artwork']['url'].replace('{w}', '220').replace('{h}', '220'),
			'image_height': 220,
			'image_width': 220,
			'error': False,
		}
	else:
		return {
			'error': True,
		}

###################################################################

def generateSpotifyOption(query):
	spotifySearchResult = spotifySearch(query)
	bestMatchHint = {}
	if spotifySearchResult['error'] == True:
		bestMatchHint = types.InlineQueryResultArticle(
			id = '1', title = "Search result for Spotify",
			description = "I've found nothing :(",
			input_message_content = types.InputTextMessageContent(message_text = "Wait"),
		)
	elif 'image' in spotifySearchResult:
		bestMatchHint = types.InlineQueryResultArticle(
			id = '1', title = "Search result for Spotify",
			description = "I've found " + spotifySearchResult['type'] + ": " + spotifySearchResult['name'],
			input_message_content = types.InputTextMessageContent(message_text = spotifySearchResult['url']),
			thumb_url = spotifySearchResult['image'], thumb_width = spotifySearchResult['image_width'], thumb_height = spotifySearchResult['image_height']
		)
	else:
		bestMatchHint = types.InlineQueryResultArticle(
			id = '1', title = "Search result for Spotify",
			description = "I've found " + spotifySearchResult['type'] + ": " + spotifySearchResult['name'],
			input_message_content = types.InputTextMessageContent(message_text = spotifySearchResult['url']),
		)
	return bestMatchHint

def generateAppleMusicOption(query):
	appleMusicSearchResult = appleMusicSearch(query)
	bestMatchHint = {}
	if appleMusicSearchResult['error'] == True:
		bestMatchHint = types.InlineQueryResultArticle(
			id = '2', title = "Search result for Apple Music",
			description = "I've found nothing :(",
			input_message_content = types.InputTextMessageContent(message_text = "Wait"),
		)
	elif 'image' in appleMusicSearchResult:
		bestMatchHint = types.InlineQueryResultArticle(
			id = '2', title = "Search result for Apple Music",
			description = "I've found " + appleMusicSearchResult['type'] + ": " + appleMusicSearchResult['name'],
			input_message_content = types.InputTextMessageContent(message_text = appleMusicSearchResult['url']),
			thumb_url = appleMusicSearchResult['image'], thumb_width = appleMusicSearchResult['image_width'], thumb_height = appleMusicSearchResult['image_height'],
		)
	else:
		bestMatchHint = types.InlineQueryResultArticle(
			id = '2', title = "Search result for Apple Music",
			description = "I've found " + appleMusicSearchResult['type'] + ": " + appleMusicSearchResult['name'],
			input_message_content = types.InputTextMessageContent(message_text = appleMusicSearchResult['url']),
		)
	return bestMatchHint


###################################################################	

@bot.message_handler(content_types=['text'])
def get_text_messages(message):
	if message.text == "Настюша":
		bot.send_message(message.from_user.id, "Настюша-хрюша")
	elif message.text == "/help":
		bot.send_message(message.from_user.id, "Напиши мне как тебя зовут")
	else:
		bot.send_message(message.from_user.id, "Я нашёл вот это:\n") # + spotifySearch(message.text)['url'])

@bot.inline_handler(func=lambda query: len(query.query) > 0)
def query_text(query):
	# print('>>>>>>> ' + str(query.query))
	spotifyOption = generateSpotifyOption(query.query)
	appleMusicOption = generateAppleMusicOption(query.query)

	bot.answer_inline_query(query.id, [spotifyOption, appleMusicOption])

bot.polling(none_stop = True, interval = 0)
